package arbolbinariobusqueda;

/**
 * @author Carlo
 * @param <E>
 */

public class ArbolAVL <E extends Comparable <E>> {
        
    //Constructor a partir de un comparador
    public ArbolAVL(Nodo<E> Nodoraiz){
        Nodoraiz = null; //Se vacía el arbol
    }
      
    public NodoAVL insertarAVL(NodoAVL Nodoraiz, NodoAVL nuevo, boolean alt, int nmMat, int id){
        NodoAVL aux;
        if(Nodoraiz == null){//El nodo raiz debe ser vacio
            Nodoraiz = nuevo;
            alt = true; //altura true
        }else
            if(id < Nodoraiz.nmMat){
        NodoAVL aux3; //asignamos un tercer auxiliar
        aux3 = insertarAVL(Nodoraiz.getNodoizquierdo(), nuevo, alt,id, nmMat);
        Nodoraiz.setNodoizquierdo(aux3);
        //Comprobamos la altura
        if(alt){//El factor de Equilibrio se reduce para aumentar la altura en la que se encuentra el nodo izquierdo
            switch(Nodoraiz.getFactorEquilibrio()){
                case 1:{
                    Nodoraiz.setFactorEquilibrio(0);
                    alt = false;
                    break;//se rompe el ciclo
                }
                case 0:{
                    Nodoraiz.setFactorEquilibrio(-1);
                    break;//no hace nada
                }
                case -1:{
                    aux = Nodoraiz.getNodoizquierdo();
                    if(aux.getFactorEquilibrio() == -1){
                        Nodoraiz = rotacionizquierda(Nodoraiz, aux);
                    }else{
                        Nodoraiz = rotacionderecho(Nodoraiz, aux);
                    }
                    alt = false;
                    }
                }
            }
        }else 
            if(id > Nodoraiz.nmMat){
                NodoAVL aux4;//asignamos un cuarto auxiliar
                aux4 = insertarAVL(Nodoraiz.getNododerecho(),nuevo,alt,id, nmMat);
                Nodoraiz.setNododerecho(aux4);
                if(alt){
                    switch(Nodoraiz.getFactorEquilibrio()){
                        case 1:{
                            aux = Nodoraiz.getNododerecho();
                            if(aux.getFactorEquilibrio() == +1){
                                Nodoraiz = rotaciondoblederecho(Nodoraiz, aux);
                            }else{
                                Nodoraiz = rotaciondobleizquierda(Nodoraiz, aux);
                            }
                            alt = false;
                            break;//rompe el ciclo
                        }
                        case 0:{
                            Nodoraiz.setFactorEquilibrio(+1);
                            break;
                        }
                        case -1:{
                            Nodoraiz.setFactorEquilibrio(0);
                            alt = false;
                        }
                    }
                }
            }
        return Nodoraiz;
    }
    
    
    //Rotacion izquierda.
    public NodoAVL rotacionizquierda(NodoAVL E, NodoAVL aux){
        E.setNodoizquierdo(aux.getNododerecho());
        aux.setNododerecho(E);
        //Actualiza el factor de Equilibrio
        if(aux.getFactorEquilibrio() == -1){
            E.setFactorEquilibrio(0);
            aux.setFactorEquilibrio(0);
        }else{
            E.setFactorEquilibrio(-1);
            aux.setFactorEquilibrio(1);
        }
        return aux;
        }
    
    //Rotacion derecho
    public NodoAVL rotacionderecho(NodoAVL E, NodoAVL aux){
        E.setNododerecho(aux.getNodoizquierdo());
        aux.setNodoizquierdo(E);
        //Actualiza el factor de Equilibrio
        if(aux.getFactorEquilibrio() == +1){
            E.setFactorEquilibrio(0);
            aux.setFactorEquilibrio(0);
        }else{
            E.setFactorEquilibrio(+1);
            aux.setFactorEquilibrio(-1);
        }
        return aux;
    }
    
    //Rotacion doble izquierdo
    public NodoAVL rotaciondobleizquierda(NodoAVL E, NodoAVL aux){
        NodoAVL aux2;
        aux2 = aux.getNodoizquierdo();
        E.setNododerecho(aux2.getNodoizquierdo());
        aux2.setNodoizquierdo(E);
        aux.setNodoizquierdo(aux2.getNododerecho());
        aux2.setNododerecho(aux);
        //Actualiza el factor de equilibrio
        if(aux2.getFactorEquilibrio() == +1){
            E.setFactorEquilibrio(-1);
        }else{
            E.setFactorEquilibrio(0);
        }
        if(aux2.getFactorEquilibrio() == -1){
            aux.setFactorEquilibrio(+1);
        }else{
            aux.setFactorEquilibrio(0);
        }
        aux2.setFactorEquilibrio(0);
        return aux2;
        }
    
    //Rotacion doble derecho
    public NodoAVL rotaciondoblederecho(NodoAVL E, NodoAVL aux){
        NodoAVL aux2;
        aux2 = aux.getNododerecho();
        E.setNodoizquierdo(aux2.getNododerecho());
        aux2.setNododerecho(E);
        aux.setNododerecho(aux2.getNodoizquierdo());
        aux2.setNodoizquierdo(aux);
        //Actualiza el factor de equilibrio
        if(aux2.getFactorEquilibrio() == +1){
            aux.setFactorEquilibrio(-1);
            }else{
            aux.setFactorEquilibrio(0);
        }
        if(aux2.getFactorEquilibrio() == -1){
            E.setFactorEquilibrio(1);
        }else{
            E.setFactorEquilibrio(0);
        }
        aux2.setFactorEquilibrio(0);
        return aux2;
        }
    
    /*
    //Metodo para insertar
    public void insertar(Nodo raiz,int E){
        if(raiz == null)
            raiz = new Nodo(E);
        else if (E < raiz.numMat)//Se compara el elemento E, si es menor que en Nodo raiz se inserta al lado izquierdo
            insertar(raiz.Nodoizquierdo,E);
        else if (E > raiz.numMat)//Se compara el elemento E, si es mayor que en Nodo raiz se inserta al lado derecho
            insertar(raiz.Nododerecho,E);
        else 
            System.out.println("Elementos iguales");
    }
    */
}
    
    
