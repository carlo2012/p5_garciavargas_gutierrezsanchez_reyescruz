package arbolbinariobusqueda;

/**
 * @author Carlo
 * @param <E>
 */

public class ArbolBinario <E extends Comparable <E>> {
    Nodo<E> Nodoraiz;
    
    
    //Metodos para recorrer PreOrden
    public void recorrerPreorden(Nodo Nodoraiz){
       if (Nodoraiz != null){
           System.out.println(Nodoraiz.elemento);
           recorrerPreorden(Nodoraiz.Nodoizquierdo);
           recorrerPreorden(Nodoraiz.Nododerecho);
       }
    } 
    
    //Metodo para recorrer InOrden
    public void recorrerInorden(Nodo Nodoraiz){
        if(Nodoraiz != null){
            recorrerInorden(Nodoraiz.Nodoizquierdo);
            System.out.println(Nodoraiz.elemento);
            recorrerInorden(Nodoraiz.Nododerecho);
        }
    }
    //Metodo para recorrer PostOrden
    public void recorrerPostorden(Nodo Nodoraiz){
        if (Nodoraiz != null){
            recorrerPostorden(Nodoraiz.Nodoizquierdo);
            recorrerPostorden(Nodoraiz.Nododerecho);
            System.out.println(Nodoraiz.elemento);
            
        }
    }
        
    @Override
    public String toString() {
        return "ArbolBinario{" + "Nodoraiz=" + Nodoraiz + '}';
    }
    private String crearRepresentacion(Nodo Nodo, String representacion, String nivel, boolean Izquierdo){
        representacion += nivel;
        if(!nivel.equals("")){
            representacion += "\b\b" + (Izquierdo ? "\u251C": "\u2514") + "\u2500";
        }
        if(Nodo == null)
            return representacion += "\n";
        representacion += Nodo + "\n";
        //Nodo izquierdo
        representacion = crearRepresentacion(Nodo.getNodoizquierdo(), representacion, nivel + "\u2502 ", true);
        //Nodo derecho
        representacion = crearRepresentacion(Nodo.getNododerecho(), representacion, nivel + " ", false);
    return representacion;
    }
}
