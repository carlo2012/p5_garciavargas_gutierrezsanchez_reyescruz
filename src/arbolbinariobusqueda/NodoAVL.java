package arbolbinariobusqueda;

/**
 *
 * @author Carlo
 * @param <E>
 */

public class NodoAVL<E> {
    //Variables
    int FactorEquilibrio;
    NodoAVL Nodoizquierdo;
    NodoAVL Nododerecho;
    E elemento;          //elemento que se va a comparar
    int id;              //identificar elemento
    int nmMat;           

    NodoAVL(Comparable E, Object object, Object object0) {
        throw new UnsupportedOperationException("Not supported yet.");    
    }
       
    //Constructores
    public void setFactorEquilibrio(int FactorEquilibrio){
        this.FactorEquilibrio = FactorEquilibrio;
    }
    
    public int getFactorEquilibrio(){
        return FactorEquilibrio;
    }
    
    
    public NodoAVL getNododerecho() {
        return Nododerecho;
    }

    public void setNododerecho(NodoAVL Nododerecho) {
        this.Nododerecho = Nododerecho;
    }

    public NodoAVL getNodoizquierdo() {
        return Nodoizquierdo;
    }

    public void setNodoizquierdo(NodoAVL Nodoizquierdo) {
        this.Nodoizquierdo = Nodoizquierdo;
    }

    public E getElemento() {
        return elemento;
    }

    public void setElemento(E elemento) {
        this.elemento = elemento;
    }
    
    public void setid(int id){
        this.id = id;
    }
    
    public int getid(){
        return id;
    }
    
    //Obtener factor.
    public int getFactor(NodoAVL E){
        if(E == null){
            return -1;
        }else{
            return E.FactorEquilibrio;
        }
    }   
        
    public void setFactor(int nuevoFactor){
        this.setFactor(nuevoFactor);   
    }
}
